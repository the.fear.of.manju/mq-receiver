package com.example.receiveMQ;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
@Configuration
@EnableRabbit
public class RabbitConfig {
    public static  String SUCCESS_QUEUE = "poc.success.queue";
    public static  String FAILURE_QUEUE = "poc.failure.queue";
    public static  String PAYMENT_QUEUE = "poc.payment.queue";
    public static  String ERROR_QUEUE = "poc.error.queue";
    public static  String ONETIME_QUEUE = "poc.onetime.queue";
    public static  String RECURRING_QUEUE = "poc.recurring.queue";

    @Bean
    ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory("localhost");
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        return connectionFactory;
    }
    @Bean
     public MessageListenerContainer messageListenerContainer() {
        SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer();
        simpleMessageListenerContainer.setConnectionFactory(connectionFactory());
        simpleMessageListenerContainer.setQueueNames(
        		SUCCESS_QUEUE, FAILURE_QUEUE, PAYMENT_QUEUE, ERROR_QUEUE, ONETIME_QUEUE, RECURRING_QUEUE
        );
        simpleMessageListenerContainer.setMessageListener(new ReceiveListner());
        return simpleMessageListenerContainer;
   }
    
}
