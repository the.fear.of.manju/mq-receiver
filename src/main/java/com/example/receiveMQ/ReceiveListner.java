package com.example.receiveMQ;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

public class ReceiveListner implements MessageListener{
	public List<Object> listScucees = new ArrayList<>();
	public List<Object> listFailuer = new ArrayList<>();
	public List<Object> listPayment = new ArrayList<>();
	public List<Object> listError = new ArrayList<>();
	public List<Object> listonetime = new ArrayList<>();
	public List<Object> listRecurring = new ArrayList<>();


    @Override
    public void onMessage(Message message){
        System.out.println("Message Receive ==========================");
        System.out.println(message);
        Object obj = new String(message.getBody());
        String queName =  message.getMessageProperties().getConsumerQueue();
        System.out.println( 
        		"This queueName is - "
        		+ message.getMessageProperties().getConsumerQueue());
	      switch (queName){
	    	case "poc.success.queue":
	    		listScucees.add(obj);
	    		break;
	    	case "poc.failure.queue":
	    		listFailuer.add(obj);
	    		break;
	    	case "poc.payment.queue":
	    		listPayment.add(obj);
	    		break;       
	    	case "poc.error.queue":
	    		listError.add(obj);
	    		break;        		
	    	case "poc.onetime.queue":
	    		listonetime.add(obj);
	    		break;       		
	    	case "poc.recurring.queue":
	    		listRecurring.add(obj);
	    		break;
	    	
	      }


    }
    public  ReceiveListner() {

        Timer timer = new Timer();  
        
        TimerTask task = new TimerTask() {
            public void run() {

                if(listScucees.size() > 0) {
                    System.out.println("success CSV : " + listScucees);
                    listScucees = new ArrayList<>();
                }
                if(listFailuer.size() > 0) {
	                System.out.println("failuer CSV : " + listFailuer);
	                listFailuer = new ArrayList<>();
                }
                if(listPayment.size() > 0) {
	                System.out.println("payment CSV : " + listPayment);
	                listPayment = new ArrayList<>();
                }
               if(listError.size() > 0) {
            	   System.out.println("error CSV : " + listError);
                	listError = new ArrayList<>();
               }
               if(listonetime.size() > 0) {
                System.out.println("onetime CSV : " + listonetime);
                listonetime = new ArrayList<>();
               }
				if(listRecurring.size() > 0) {
				    System.out.println("recurring CSV : " + listRecurring);
				    listRecurring = new ArrayList<>();
				}
         }

        };
        timer.scheduleAtFixedRate(task,5000, 5000); // 今回追加する処理
    }

}
